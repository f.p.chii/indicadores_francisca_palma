
require( 'console-stamp' )( console );

const readline = require('readline');
var datos = require("./datos/actualizarDatos")
var calculos = require("./estadisticas/calculos");
var io = require("./io/io");

var sc = readline.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
);

function menu(){

    
    console.log('###--------------------Menú-------------------###');
    console.log('1. Actualizar datos');  
    console.log('2. Promediar datos');
    console.log('\t2.1 Dolar\t2.2 Euro\t2.3 Tasa de desempleo');  
  
    console.log('3. Valor más actual');
    console.log('\t3.1 Dolar\t3.2 Euro\t3.3 Tasa de desempleo');  
    
    console.log('4. Mostrar mínimos históricos');
    console.log('\t4.1 Dolar\t4.2 Euro\t4.3 Tasa de desempleo');  

    console.log('5. Mostrar máximos históricos');
    console.log('\t5.1 Dolar\t5.2 Euro\t5.3 Tasa de desempleo');  
    
    console.log('6. Salir');
    console.log('Elija una opción ');
    sc.question('',opc => {
        switch (opc) {
            case '1': datos.actualizarDatos(); menu(); break;

            case '2.1': calculos.promediar('dolar'); menu(); break;
            case '2.2': calculos.promediar('euro'); menu(); break;
            case '2.3': calculos.promediar('tasa_desempleo'); menu(); break;
            
            case '3.1': io.masActual('dolar'); menu(); break;
            case '3.2': io.masActual('euro'); menu(); break;
            case '3.3': io.masActual('tasa_desempleo'); menu(); break;

            case '4.1': calculos.minimosHistoricos('dolar'); menu(); break;
            case '4.2': calculos.minimosHistoricos('euro'); menu(); break;
            case '4.3': calculos.minimosHistoricos('tasa_desempleo'); menu(); break;

            case '5.1': calculos.maximosHistoricos('dolar');menu(); break;
            case '5.2': calculos.maximosHistoricos('euro');menu(); break;
            case '5.3': calculos.maximosHistoricos('tasa_desempleo');menu(); break;

            case '6':
                console.log('Cerrando la aplicación...');
                sc.close();
                process.exit(0);
                break;
            default: console.log('[Atención] Elija una opción válida'); menu(); break;
        }
    })
}



menu();