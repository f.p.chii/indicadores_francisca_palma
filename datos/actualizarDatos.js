const fetch = require('node-fetch');

const io = require('../io/io');


const stat = resp =>
{
    if(resp.status >= 200 && resp.status < 300)
    {
        return Promise.resolve(resp);
    }

    return Promise.reject(new Error(resp.statusText));
};

const obtenerJson = resp => {
    return resp.json();

}

const extraerNecesario = resp =>
{
    
    var dolar = resp.dolar;
    var euro = resp.euro;
    var tasa_desempleo  = resp.tasa_desempleo;
    
    return {dolar,euro,tasa_desempleo};
}

const guardarEnArchivo = resp =>{
    io.guardarDatos(resp);
}


function actualizar ()
{
    return fetch('https://mindicador.cl/api')
    .then(stat)
    .then(obtenerJson)
    .then(extraerNecesario)
    .then(guardarEnArchivo)
    .catch(errores);
};

var errores = function (err) {
    console.log("[Atención] No se puede actualizar los indicadores, ups :c")
    
}


module.exports = {
    actualizarDatos :  actualizar,
};