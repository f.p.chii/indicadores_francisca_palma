const fs = require('fs');
var path = require('path');

var guardar = function (datos) {

    fs.writeFile('./datos/' + Date.now() + '.ind', JSON.stringify(datos, null, "\t"), function (err) {
        if (err) throw err;
    });
}


var masActual = function (indicador) {
    
    /**esta parte busca entre los archivos existentes cual es el más actual (mayor) */
    fs.readdir('datos', function (err, items) {
        if(err){
            console.log(err);   
        }
        else if (items && items.length > 1) { /* si hay archivos */
            var actual = 0;
            for (const element of items) {
                var num = Number(element.slice(0, element.length - 4));
                if (num > actual) {
                    actual = num;
                }
            }
            /* acá ya se sabe cual es el mayor */
            /* leer archivo y sacar el "indicador" */
            var path = 'datos/'+actual+'.ind';
            fs.readFile(path, 'utf-8', (err, data) => {
                if (err) {
                    console.log('error: ', err);
                } else {
                    
                    var datos = JSON.parse(data)[indicador];
                    var fecha = new Date(actual).toLocaleString();
                    console.log('Los datos fueron extraídos: '+fecha);
                    console.log('Valor más Actual: '+datos.nombre +':'+ datos.valor);
                }
            });
            

        }
        else {
            console.log("Primero debe actualizar datos");

        }

    });
}

var manejarArchivos = function () {  
    var archivos = fs.readdirSync('datos');
    //pone en la lista los archivos con extension .ind
    var filteredList = archivos.filter(function(e){
        return path.extname(e).toLowerCase() === '.ind'
    });
    //debe leer cada archivo
    return Promise.resolve( filteredList);

}

var obtenerDatos = (resp,indicador) =>{
    var datos = resp.map(element => {
        return JSON.parse(fs.readFileSync('datos'+'/'+element, 'utf-8'))[indicador];
    });
    return datos;

}


var obtenerTodosValores = function (indicador) {
    return manejarArchivos()
    .then(data => obtenerDatos(data,indicador))
    
}


module.exports = {
    masActual: masActual,
    guardarDatos: guardar,
    obtenerTodosLosValores: obtenerTodosValores
};