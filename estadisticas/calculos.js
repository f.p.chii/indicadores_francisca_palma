
var io = require('../io/io');

var promediar = function (indicador) {
    
    io.obtenerTodosLosValores(indicador)
    .then(promediaryFecha);
}
var promediaryFecha = function (arr) {
    if(!arr){
        console.log("No hay datos para calcular el promedio");
        
        return;
    }
    var suma = 0;
    var n = arr.length;
    var minDate = arr[0].fecha //antes
    var maxDate = arr[0].fecha //despues
    for (const iterator of arr) {
        suma+= parseFloat(iterator.valor)
        if(iterator.fecha > maxDate){
            maxDate = iterator.fecha;
        }
        if(iterator.fecha < minDate){
            minDate = iterator.fecha;
        }
    }
    console.log("Rango: " + new Date(minDate).toLocaleDateString() +' a '+ new Date(maxDate).toLocaleDateString());
    console.log('Promedio: '+arr[0].nombre+' fue de '+ parseFloat(suma/n).toFixed(2) );    
    
}
var minimos = function (indicador) {
    io.obtenerTodosLosValores(indicador)
    .then(buscarMinimoyFecha)
    //.then(console.log);    
}

var buscarMinimoyFecha = function (array) {
    var min = array[0].valor;
    var valDate = array[0].fecha 
    var minDate = array[0].fecha //antes
    var maxDate = array[0].fecha //despues
    for (const iterator of array) {
        if(iterator.valor < min){
            min =iterator.valor;
            valDate = iterator.fecha;
        }
        if(iterator.fecha > maxDate){
            maxDate = iterator.fecha;
        }
        if(iterator.fecha < minDate){
            minDate = iterator.fecha;
        }
    }
    console.log("Rango: " + new Date(minDate).toLocaleDateString() +' a '+ new Date(maxDate).toLocaleDateString());
    console.log('Minimo histórico: ' + array[0].nombre+' a '+ min + ' el '+  new Date(valDate).toLocaleDateString());    
}

var maximos = function (indicador) {
    io.obtenerTodosLosValores(indicador)
    .then(buscarMaximoyFecha)

    
}
var buscarMaximoyFecha = function (array) {
    var max = array[0].valor;
    var valDate = array[0].fecha 
    var minDate = array[0].fecha //antes
    var maxDate = array[0].fecha //despues
    for (const iterator of array) {
        if(iterator.valor > max){
            max =iterator.valor;
            valDate = iterator.fecha;
        }
        if(iterator.fecha > maxDate){
            maxDate = iterator.fecha;
        }
        if(iterator.fecha < minDate){
            minDate = iterator.fecha;
        }
    }
    console.log("Rango: " + new Date(minDate).toLocaleDateString() +' a '+ new Date(maxDate).toLocaleDateString());
    console.log('Máximo histórico: ' +array[0].nombre+' a '+ max + ' el '+  new Date(valDate).toLocaleDateString());    
}

module.exports = {
    promediar :  promediar,
    minimosHistoricos: minimos,
    maximosHistoricos: maximos
};